var img = new Image();
img.src = '/static/test.jpg'; // image url
img.onload = function() {
  var cvs = document.createElement('canvas');
  var ctx = cvs.getContext('2d');
  cvs.width = img.width;
  cvs.height = img.height;
  ctx.width = img.width;
  ctx.height = img.height;
  ctx.canvas.width = img.width;
  ctx.canvas.height = img.height;
  ctx.drawImage(img, 0, 0);

  var base64 = cvs.toDataURL();
 
}
