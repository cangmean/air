# coding:utf-8

from flask import Blueprint
from flask_restful import Api
from .public import QrSaveApi, QrCheckApi, QrScanApi, IndexApi

bp = Blueprint('api', __name__, url_prefix='/api')
api = Api(bp)

# public
api.add_resource(IndexApi, '/index')
api.add_resource(QrSaveApi, '/save')
api.add_resource(QrCheckApi, '/check')
api.add_resource(QrScanApi, '/scan')
