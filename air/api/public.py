# coding:utf-8

import json
import base64
from flask_restful import Resource
from flask import request
from air.utils.lean import QrState
from StringIO import StringIO


class IndexApi(Resource):

    def get(self):
        return {'result': 'ok'}

    def post(self):
        return {'result': 'ok'}


class QrSaveApi(Resource):

    def get(self):
        pass

    def post(self):
        data = request.files.get('fl')
        data = data.read()
        qs = QrState()
        result = qs.upload(data)
        return result


class QrCheckApi(Resource):

    def get(self):
        id = request.args.get('id')
        result = QrState.check(id)
        return result


class QrScanApi(Resource):

    def get(self):
        id = request.args.get('id')
        result = QrState.scan(id)
        return result
