# coding:utf-8

import string
from flask import Blueprint, request, render_template
from air.utils.niu import upload

bp = Blueprint('home', __name__)


@bp.route('/', methods=['GET', 'POST'])
def index():

    if request.method == 'POST':
        fl = request.files.get('fl')
        url = upload('bucket', fl.filename, fl.stream.read(), flag='2')
    return render_template('index.html')
