# coding:utf-8

from flask import Flask
from air.extends import api


def create_app():
    app = Flask(__name__)

    api.init_app(app)

    register_blueprints(app)

    @app.after_request
    def after_request(response):
        response.headers.add('Access-Control-Allow-Origin', '*')
        response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
        response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
        return response

    return app


def register_blueprints(app):
    from air import api
    from air.views import home
    for i in (api, home):
        app.register_blueprint(i.bp)
