# coding:utf-8

import base64
from air.config import config
from qiniu import Auth, put_file, put_data

AK = config['qiniu']['AK']
SK = config['qiniu']['SK']
base_url = config['qiniu']['base_url']

q = Auth(AK, SK)


def get_token(bucket_name, key, expires=3600):
    token = q.upload_token(bucket_name, key, expires)
    return token

def upload(bucket_name, key, path, flag='1'):
    '''
    param:bucket_name 要上传的空间
    param:key 上传岛骑牛后保存的文件名
    param:path 上传文件的本地路径
    param:flag 上传文件方式 1:表示文件上传, 2: 表示流式上传
    '''
    token = get_token(bucket_name, key)
    wrapper = {'1': put_file, '2': put_data}
    ret, info = wrapper[flag](token, key, path)
    url = 'http://%s/%s' % (base_url, key)
    url = base64.b64encode(url)
    return url


if __name__ == '__main__':
    upload('bucket', 'hello.jpg', 'pic.jpg')

