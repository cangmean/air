# coding:utf-8

import leancloud
import base64
from leancloud import Object, Query, File, LeanCloudError
from air.config import config
from StringIO import StringIO

leancloud.init(config['leancloud']['id'],  config['leancloud']['key'])
#leancloud.init(config['leancloud']['id'],  master_key='8CLgmg60KmVA9A5dvkc76iRu')


class QrState(Object):

    def upload(self, data):
        fl = File('qr.jpg', StringIO(buffer(data)), 'image/jpeg')
        fl.save()
        self.set('image', fl)
        self.set('status', 0)
        self.save()
        return {'url': fl.url, 'id': str(self.id)}
    
    @classmethod
    def check(cls, id):
        query = Query(cls)
        obj = query.get(id)
        return {'status': obj.get('status')}

    @classmethod
    def scan(cls, id):
        query = Query(cls)
        obj = query.get(id)
        obj.set('status', 1)
        obj.save()
        return {}
